package edu.uchicago.gerber.lec04;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

/**
 * Created by ag on 4/22/2015.
 */
public class AnimActivity extends ActionBarActivity {

    private LinearLayout ll;
    private float startX;
    private Animation animLeft;
    private Animation animRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slider_layout);

        ll = (LinearLayout) findViewById(R.id.slider);
        ll.setVisibility(View.GONE);

        animLeft =
                AnimationUtils.loadAnimation(this, R.anim.anim_left);
        animRight = AnimationUtils.loadAnimation(this, R.anim.anim_right);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                startX = event.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                float endX = event.getX();

                if (endX < startX) {
                    System.out.println("Move left");
                    ll.setVisibility(View.VISIBLE);
                    ll.startAnimation(animLeft);
                }
                else {
                    ll.startAnimation(animRight);
                    ll.setVisibility(View.GONE);
                }
            }

        }
        return true;
    }

}