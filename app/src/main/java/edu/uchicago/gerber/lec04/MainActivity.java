package edu.uchicago.gerber.lec04;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private Button mButtonFlip;
    private boolean bFlip = false;
    private Fragment mFragmentOne, mFragmentTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, this.getClass().getCanonicalName(), Toast.LENGTH_SHORT).show();


        getSupportActionBar().setTitle("Lecture 04");

        if (savedInstanceState == null) {

            mFragmentOne = new FirstFragment();
            mFragmentTwo = new SecondFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_right,mFragmentOne)
                    .commit();


            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_left,mFragmentTwo)
                    .commit();
        }










    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FirstFragment extends Fragment {

        private Button mButtonFirst;

        public FirstFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.first_frag, container, false);

            mButtonFirst = (Button) rootView.findViewById(R.id.btnFirst);
            mButtonFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(getActivity(), "from first button", Toast.LENGTH_SHORT).show();
                    ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("First");
                }
            });

            return rootView;
        }
    }

    public static  class SecondFragment extends Fragment {

        private Button mButtonSecond;

        public SecondFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.second_frag, container, false);

            mButtonSecond = (Button) rootView.findViewById(R.id.btnSecond);
            mButtonSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Second");
                }
            });


            return rootView;
        }
    }
}
